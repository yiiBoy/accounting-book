
## FOLDER STRUCTURE  
##
```
    ./bin/
        back - ready to deploy nodejs application
        front - ready to deploy react application
    ./src/
        back - source code of backend application
        front - source code of frontend application
```

## QUICK START

### RUN REST API WEB SERVICE
###
```
    npm start --prefix bin/back
```

### RUN REACT WEB APPLICATION
###

The bin/front folder is ready to be deployed.  
You may serve it with a static server:  
###

```
    npm install -g serve
    serve -s bin/front
```

Find out more about deployment here:

  https://cra.link/deployment
