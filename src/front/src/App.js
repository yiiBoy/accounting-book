import 'bootstrap/dist/css/bootstrap.min.css';
// import logo from './logo.svg';
import './App.css';
import { useTable, useExpanded } from 'react-table';
import React, { useState, useEffect, useCallback } from 'react';

const apiUrl = prompt('Enter API url', 'http://localhost:3000');

function App() {
    const columns = React.useMemo(
        () => [
            // {
            //     // Make an expander cell
            //     Header: () => null, // No header
            //     id: 'expander', // It needs an ID
            //     Cell: ({ row }) => (
            //         // Use Cell to render an expander for each row.
            //         // We can use the getToggleRowExpandedProps prop-getter
            //         // to build the expander.
            //         // <span {...row.getToggleRowExpandedProps()}>
            //         <span>
            //             {row.isExpanded ? '👇' : '👉'}
            //         </span>
            //     ),
            // },
            {
                Header: 'Type',
                accessor: 'type',
            },
            {
                Header: 'Amount',
                accessor: 'amount',
            },
        ],
        []
    );

    const [data, setData] = useState({
        isLoaded: false,
        data: null,
    });

    useEffect(() => {
        async function fetchData() {
            fetch(apiUrl + '/transactions')
                .then((response) => {
                    if (!response.ok) {
                        throw new Error(`HTTP error: ${response.status}`);
                    }
                    // если HTTP-статус в диапазоне 200-299
                    return response.json();
                })
                .then((data) => {
                    setData({
                        isLoaded: true,
                        data: data.transactions,
                    });
                })
                .catch((e) => {
                    alert(e.message);
                });
        }
        fetchData();
    }, []);

    // Create a function that will render our row sub components
    const renderRowSubComponent = useCallback(({ row }) => {
        return (
            <div>
                <table className='table'>
                    {Object.entries(row.original).map((item) => (
                        <tr>
                            <td>{item[0]}:</td>
                            <td>{item[1]}</td>
                        </tr>
                    ))}
                </table>
                {/* <pre
                    style={{
                        fontSize: '10px',
                    }}
                >
                    <code>{JSON.stringify(row.original, null, 2)}</code>
                </pre> */}
            </div>
        );
    }, []);

    return (
        <div className='App container-fluid'>
            <h1>Accounting test application</h1>

            {!data.isLoaded && (
                <div>
                    <div className='text-center'>
                        <div>Loading...</div>
                        <div className='spinner-border spinner-border-lg align-center'></div>
                    </div>
                </div>
            )}
            {data.isLoaded && (
                <div>
                    {/* <pre>
                        <code>{JSON.stringify(data, null, 2)}</code>
                    </pre> */}
                    <Table
                        columns={columns}
                        data={data.data}
                        renderRowSubComponent={renderRowSubComponent}
                    />
                </div>
            )}
        </div>
    );
}

function Table({ columns: userColumns, data, renderRowSubComponent }) {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        visibleColumns,
        state: { expanded },
    } = useTable(
        {
            columns: userColumns,
            data,
        },
        useExpanded // We can useExpanded to track the expanded state
        // for sub components too!
    );

    return (
        <>
            {/* <pre>
                <code>{JSON.stringify({ expanded: expanded }, null, 2)}</code>
            </pre> */}
            <table {...getTableProps()} className='table -table-striped'>
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column) => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {rows.map((row, i) => {
                        prepareRow(row);
                        return (
                            // Use a React.Fragment here so the table markup is still valid
                            <React.Fragment {...row.getRowProps()}>
                                <tr
                                    {...row.getToggleRowExpandedProps()}
                                    style={{
                                        backgroundColor: row.original.type=='debit'?'lightblue':'pink',
                                    }}
                                >
                                    {row.cells.map((cell) => {
                                        return (
                                            <td {...cell.getCellProps()}>
                                                {cell.render('Cell')}
                                            </td>
                                        );
                                    })}
                                </tr>
                                {/*
                                    If the row is in an expanded state, render a row with a
                                    column that fills the entire length of the table.
                                */}
                                {row.isExpanded ? (
                                    <tr>
                                        <td
                                            colSpan={visibleColumns.length}
                                            style={{
                                                backgroundColor: '#EEE',
                                            }}
                                        >
                                            {/*
                                            Inside it, call our renderRowSubComponent function. In reality,
                                            you could pass whatever you want as props to
                                            a component like this, including the entire
                                            table instance. But for this example, we'll just
                                            pass the row
                                            */}
                                            {renderRowSubComponent({ row })}
                                        </td>
                                    </tr>
                                ) : null}
                            </React.Fragment>
                        );
                    })}
                </tbody>
            </table>
            <br />
            <div>Showing the first 20 results of {rows.length} rows</div>
        </>
    );
}

export default App;
