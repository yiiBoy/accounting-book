# Accounting test application

## QUICK START
To run application use
```js
    npm start
```

## REQUESTS

### BALANCE
```js
    fetch('http://localhost:3000', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(console.log)
```

### TRANSACTIONS
```js
    fetch('http://localhost:3000/transactions', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(console.log)
```

### TRANSACTION
```js
    fetch('http://localhost:3000/transactions/<transaction_id>', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(console.log)
```

### DEBIT
```js
    fetch('http://localhost:3000/transactions', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            type: 'debit',
            amount: 10
        })
    })
    .then(res => res.json())
    .then(console.log)
```

### CREDIT
```js
    fetch('http://localhost:3000/transactions', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            type: 'credit',
            amount: 10
        })
    })
    .then(res => res.json())
    .then(console.log)
```
