var express = require('express');
const accountService = require('../services/account');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    // res.render('index', { title: 'Express });
    res.json({
        result: 'success',
        balance: accountService.getBalance(),
    });
});

module.exports = router;
