const accountService = require('../services/account');
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../middleware/validate-request');

/* GET transactions listing. */
router.get('/', function (req, res, next) {
    res.send({
        result: 'success',
        transactions: accountService.getAllTransactions(),
    });
});

function transactionSchema(req, res, next) {
    const schema = Joi.object({
        type: Joi.string().required().valid('credit', 'debit'),
        amount: Joi.number().required().greater(0),
    });
    validateRequest(req, res, next, schema);
}

router.post('/', transactionSchema, function (req, res, next) {
    const { amount, type } = req.body;

    accountService
        .createTransaction(amount, type)
        .then(() => {
            res.json({
                result: 'success',
                balance: accountService.getBalance(),
            });
        })
        .catch((e) => {
            res.status(422).json({
                result: 'error',
                message: 'Unprocessable Entity',
                data: req.body,
            });
        });
});

router.get('/:id', function (req, res, next) {
    const trx = accountService.getTransactionById(req.params.id);
    if (trx) {
        return res.json(trx);
    } else {
        return res.status(404).json({
            result: 'error',
            message: 'Not found',
        });
    }
});

module.exports = router;
